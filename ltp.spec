Name     : ltp
Version  : 20200120
Release  : 27
URL      : https://linux-test-project.github.io/
Source0  : https://github.com/linux-test-project/ltp/releases/download/20200120/ltp-full-20200120.tar.xz
Summary  : Test tool for driving IO to block, raw, filesystem targets
Group    : Development/Tools
License  : GPL-2.0

%define debug_package %{nil}

%description
This package provides the disk testing utility for performing IO testing to
block, raw, and filesystem targets.

Authors:
--------
    Brent Yardley <yardleyb@us.ibm.com>

%prep
%setup -q -n ltp-full-20200120

%build
export CFLAGS="$CFLAGS -fno-lto"
make autotools
make V=1 %{?_smp_mflags}

%install
rm -rf %{buildroot}
%make_install
chmod 0755 %{buildroot}/opt/ltp/bin/*
chmod 0755 %{buildroot}/opt/ltp/testcases/bin/*

%files
%defattr(-,root,root,-)
/opt/ltp/IDcheck.sh
/opt/ltp/Version
/opt/ltp/bin/create_dmesg_entries_for_each_test.awk
/opt/ltp/bin/create_kernel_faults_in_loops_and_probability.awk
/opt/ltp/bin/create_valgrind_check.awk
/opt/ltp/bin/execltp
/opt/ltp/bin/genhtml.pl
/opt/ltp/bin/html_report_header.txt
/opt/ltp/bin/insert_kernel_faults.sh
/opt/ltp/bin/ltp-bump
/opt/ltp/bin/ltp-pan
/opt/ltp/bin/make-file.sh
/opt/ltp/bin/restore_kernel_faults_default.sh
/opt/ltp/runltp
/opt/ltp/runtest/*
/opt/ltp/scenario_groups/default
/opt/ltp/scenario_groups/network
/opt/ltp/share/man/man1/doio.1
/opt/ltp/share/man/man1/iogen.1
/opt/ltp/share/man/man1/ltp-bump.1
/opt/ltp/share/man/man1/ltp-pan.1
/opt/ltp/share/man/man3/parse_opts.3
/opt/ltp/share/man/man3/parse_ranges.3
/opt/ltp/share/man/man3/random_range.3
/opt/ltp/share/man/man3/random_range_seed.3
/opt/ltp/share/man/man3/tst_res.3
/opt/ltp/share/man/man3/tst_sig.3
/opt/ltp/share/man/man3/tst_tmpdir.3
/opt/ltp/share/man/man3/usctest.3
/opt/ltp/testcases/bin/*
/opt/ltp/testcases/data/*
/opt/ltp/testscripts/*
/opt/ltp/ver_linux
